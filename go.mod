module bitbucket.org/entrlcom/zodiacsignpb

go 1.17

require bitbucket.org/entrlcom/open-genproto v0.0.0-20220621101603-f6a4ea079be7

require (
	github.com/google/go-cmp v0.5.7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
