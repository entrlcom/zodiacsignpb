package zodiacsignpb

import (
	"strings"
	"time"

	"bitbucket.org/entrlcom/open-genproto/gen/go/zodiacsign/v1"
)

const prefix = "ZODIAC_SIGN_"

func NewFromString(s string) zodiacsign.ZodiacSign {
	s = strings.ToUpper(s)
	if strings.HasPrefix(s, prefix) {
		return zodiacsign.ZodiacSign(zodiacsign.ZodiacSign_value[s])
	}

	return zodiacsign.ZodiacSign(zodiacsign.ZodiacSign_value[prefix+s])
}

func NewFromTime(t time.Time) zodiacsign.ZodiacSign {
	if t.IsZero() {
		return zodiacsign.ZodiacSign_ZODIAC_SIGN_UNSPECIFIED
	}

	switch t.Month() {
	case time.January:
		if t.Day() < 21 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_CAPRICORN
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_AQUARIUS
	case time.February:
		if t.Day() < 19 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_AQUARIUS
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_PISCES
	case time.March:
		if t.Day() < 21 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_PISCES
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_ARIES
	case time.April:
		if t.Day() < 21 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_ARIES
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_TAURUS
	case time.May:
		if t.Day() < 22 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_TAURUS
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_GEMINI
	case time.June:
		if t.Day() < 22 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_GEMINI
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_CANCER
	case time.July:
		if t.Day() < 23 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_CANCER
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_LEO
	case time.August:
		if t.Day() < 24 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_LEO
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_VIRGO
	case time.September:
		if t.Day() < 23 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_VIRGO
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_LIBRA
	case time.October:
		if t.Day() < 24 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_LIBRA
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_SCORPIO
	case time.November:
		if t.Day() < 23 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_SCORPIO
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_SAGITTARIUS
	case time.December:
		if t.Day() < 22 {
			return zodiacsign.ZodiacSign_ZODIAC_SIGN_SAGITTARIUS
		}

		return zodiacsign.ZodiacSign_ZODIAC_SIGN_CAPRICORN
	}

	return zodiacsign.ZodiacSign_ZODIAC_SIGN_UNSPECIFIED
}

func ToString(zs zodiacsign.ZodiacSign) string {
	return strings.ReplaceAll(zs.String(), prefix, "")
}
